#!/bin/sh

# xorg-utils
sudo pacman -S xorg-font-util xorg-fonts-100dpi xorg-fonts-75dpi xorg-mkfontscale xorg-xinit xorg-xprop xorg-xrandr xorg-xwininfo --noconfirm

# script-utils
sudo pacman -S wget jq zip unzip --noconfirm

# necessities
sudo pacman -S pulseaudio pulseaudio-alsa pavucontrol pacman-contrib exa bat fd procs man-db neofetch --noconfirm

# wm-utils
sudo pacman -S xmonad xmonad-contrib picom alacritty --noconfirm

# font-utils
sudo pacman -S noto-fonts ttf-hack ttf-joypixels --noconfirm

# general-utils
sudo pacman -S ffmpeg mpv maim hsetroot discord --noconfirm

# surf-utils
sudo pacman -S aria2 woff2 webkit2gtk nuspell libvoikko libtool hunspell hspell gstreamer gtk2 gst-plugins-good gst-libav gcr aspell ghc ghc-libs gst-plugins-base-libs --noconfirm

# logs
sudo pacman -S syslog-ng-runit --noconfirm

# remove neovim
sudo pacman -Rns neovim --noconfirm

# aur repos
paru -S xdg-user-dirs sxiv rsm neovim-git ripgrep rm-improved shell-color-scripts --noconfirm
