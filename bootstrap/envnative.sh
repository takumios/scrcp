#!/bin/sh

# variables
CLONE="git clone https:/"
GLCLONE="git clone https://gitlab.com"
GHCLONE="git clone https://github.com"

# directory structure
declare -a DIRS=(
"$HOME/dox"
"$HOME/dox/dl"
"$HOME/dox/drop"
"$HOME/dox/dsk"
"$HOME/dox/vids"
"$HOME/dox/vids/ffmpeg"
"$HOME/repo"
)

while [ -z "$d" ]; do
    echo ""
    for d in "${DIRS[@]}"; do
        [ -d "$d" ] && echo "abstain creation of \"$d\", directory already exists." || mkdir $d
        sleep 0.1
    done
echo -e "\nDirectory-Structure: SUCCESSFUL\n"
sleep 1
done

# aur helper
if [ -f "/usr/bin/paru" ]; then
    echo -e "abstain creation of \"paru\", aur-helper already exists.\n"
    sleep 1
else
    cd $HOME/repo/ && $CLONE/aur.archlinux.org/paru-git &&
    cd $HOME/repo/paru-git/ && makepkg -si
fi
echo -e "Aur-Helper: SUCCESSFUL\n"
sleep 1

# src repos
declare -a REPOS=(
"$HOME/repo/dmenu-patched"
"$HOME/repo/surf-patched"
"$HOME/repo/spotifyd-fix"
"$HOME/repo/pipe-viewer"
"$HOME/repo/spotify-tui"
)

cd $HOME/repo/
while [ -z "$r" ]; do
    for r in "${REPOS[@]}"; do
        if [ -d "$r" ]; then
            echo "abstrain creation of \"$r\", directory already exists."
            sleep 0.1
        else
            BASE=$(basename $r)
            case $BASE in $r) echo "";;
                dmenu-patched)
                    $GLCLONE/zachens/dmenu-patched;;
                surf-patched)
                    $GLCLONE/zachens/surf-patched;;
                spotifyd-fix)
                    $GLCLONE/zachens/spotifyd-fix;;
                pipe-viewer)
                    $GHCLONE/trizen/pipe-viewer;;
                spotify-tui)
                    $GHCLONE/Rigellute/spotify-tui;;
                    *)
            esac
        fi
    done
echo -e "\nRepository-Sources: SUCCESSFUL\n"
sleep 1
done

# etc. repos
cd $HOME/dox/
[ -d "$HOME/dox/pix" ] && (echo "abstrain creation of \"pix\", directory already exists." ; sleep 0.1) ||
    ($GLCLONE/zachens/pix-qfhd ; mv pix-qfhd pix)
[ -d "$HOME/dox/scrcp" ] && (echo "abstrain creation of \"scrcp\", directory already exists." ; sleep 0.1) ||
    ($GLCLONE/zachens/scrcp-all ; mv scrcp-all scrcp)
echo -e "\nDox-Repository: SUCCESSFUL\n"
sleep 1

# dotfile setup
cd $HOME/dox/drop/
[ -d "$HOME/dox/drop/dtfls-rndm" ] && (echo -e "abstrain creation of \"dtfls-rndm\", directory already exists." ; sleep 0.1) ||
    $GLCLONE/zachens/dtfls-rndm

cd $HOME/dox/drop/dtfls-rndm/
mv -f $HOME/dox/drop/dtfls-rndm/.bashrc $HOME/dox/drop/dtfls-rndm/.bash_profile $HOME/dox/drop/dtfls-rndm/.config/ $HOME/dox/drop/dtfls-rndm/.xmonad/ $HOME/
cd $HOME/

[ -f "$HOME/.xinitrc" ] && (echo -e "abstrain creation of \".xinitrc\", file already exists.\n" ; sleep 0.1) ||
    ln -s $HOME/.config/x11/xinitrc $HOME/.xinitrc

[ -d "$HOME/dox/drop/dtfls-rndm" ] && rm -rf $HOME/dox/drop/dtfls-rndm

echo -e "Dotfiles: SUCCESSFUL\n"
sleep 1


# package setup
sudo mv $HOME/.config/temps/pacman.conf /etc/

sudo pacman -Syu --noconfirm
[ -f $HOME/dox/scrcp/bootstrap/packages.sh ] && sh $HOME/dox/scrcp/bootstrap/packages.sh ||
    echo "execution aborted, \"packages.sh\" cannot be located."
paru -Syu --noconfirm
echo -e "\nPackages: SUCCESSFUL\n"
sleep 1

# configs
sudo pacman -Syu --noconfirm
nvim -c :PlugInstall
mv -f $HOME/.config/onedark-vim/onedark.vim $HOME/.config/nvim/autoload/plugged/onedark.vim/autoload/
curl -fsSL https://starship.rs/install.sh | bash
sudo mv -f $HOME/.config/colorscript/colorscript /usr/bin/colorscript
sudo rm -rf /opt/shell-color-scripts/colorscripts

# building
cd ~/repo/dmenu-patched/
sudo make clean install
cd ~/repo/surf-patched/
sudo make clean install
cd ~/repo/spotify-tui/
cargo build --release
cargo install --path .
cd ~/repo/spotifyd-fix/
cargo build --release --features "pulseaudio_backend"
cargo install --path .
cpan Module::Build
cd ~/repo/pipe-viewer/
perl Build.PL
sudo ./Build installdeps ; sudo ./Build install

# services
sudo rsm enable syslog-ng
sudo rsm start syslog-ng

sudo rm -rf $HOME/.bash_history $HOME/.bash_logout $HOME/.cpan

